<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:if test="${not (empty user)}">
	<p>You are authenticated as ${user.id}</p>
	<p><a href="LogoutServlet">Logout</a></p>
</c:if>
<c:if test="${empty user}">
	<a href="LoginServlet">Login</a>
</c:if>
<p style="color: red;">${message}</p>
<h3>
	<a href="ResearchersListServlet">Researchers list</a>
</h3>
<hr>
</body>
</html>