<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

<form action="CreateResearcherServlet" method="post">
	<input type="text" name="id" placeholder="User Id"> 
	<input type="text" name="name" placeholder="Name">
	<input type="text" name="lastname" placeholder="Last name">
	<input type="text" name="email" placeholder="Email">
	<input type="text" name="password" placeholder="Password">
	<button type="submit">Create researcher</button>
</form>

<h2>Populate researchers</h2>
	<form action="PopulateResearchersServlet" method="post"
		enctype="multipart/form-data">
		<input type="file" name="file" />
		<button type="submit">Populate</button>
	</form>


</body>
</html>