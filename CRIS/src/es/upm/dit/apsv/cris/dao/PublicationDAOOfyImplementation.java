package es.upm.dit.apsv.cris.dao;

import java.util.List;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class PublicationDAOOfyImplementation implements PublicationDAO {
	
	private static PublicationDAOOfyImplementation instance = null;
	private PublicationDAOOfyImplementation() {}
	public static PublicationDAOOfyImplementation getInstance() {
		if( null == instance ) {
			instance = new PublicationDAOOfyImplementation();
		}
		return instance;
	}

	@Override
	public Publication create(Publication publication) {
		ofy().save().entity(publication).now();
		return publication;
	}

	@Override
	public Publication read(String publicationId) {
		Publication publication = null;
		ofy().load().type(Publication.class).id(publicationId).now();
		return publication;
	}

	@Override
	public Publication update(Publication publication) {
		ofy().save().entity(publication).now();
		return publication;
	}

	@Override
	public Publication delete(Publication publication) {
		ofy().delete().entity(publication).now();
		return publication;
	}

	@Override
	public List<Publication> readAll() {
		return ofy().load().type(Publication.class).list();
	}


}
