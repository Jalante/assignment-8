package es.upm.dit.apsv.cris.dao;

import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Researcher;

public class ResearcherDAOImplementation implements ResearcherDAO {

	private static ResearcherDAOImplementation instance = null;
	private ResearcherDAOImplementation() {}
	public static ResearcherDAOImplementation getInstance() {
		if( null == instance ) {
			instance = new ResearcherDAOImplementation();
		}
		return instance;
	}

	public Researcher create( Researcher researcher ) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.save(researcher);
			session.getTransaction().commit();
		} catch (Exception e) {
			// handle exceptions
		} finally {
			session.close();
		}
		return researcher;
	};
	
	public Researcher read(String researcherId ) {
		Researcher researcher = null;
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			researcher = session.get(Researcher.class, researcherId);
			session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
		return researcher;
	};

	public Researcher update( Researcher researcher ) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(researcher);
			session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
		return researcher;
	};
	
	public Researcher delete( Researcher researcher ) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(researcher);
			session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
		return researcher;
	};
	
	public List<Researcher> readAll(){
		List<Researcher> researchers = null;
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			researchers = (List<Researcher>) session.createQuery("from Researcher").list();
					session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
		return researchers;
	};
	
	public Researcher readAsUser(String email, String password) {
		Researcher researcher = null;
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			researcher = (Researcher) session.createQuery("select r from Researcher r where r.email= :email and r.password = :password")
				.setParameter("email", email)
				.setParameter("password", password)
				.uniqueResult();
			
			session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
		session.close();
	}
		return researcher;
}}

