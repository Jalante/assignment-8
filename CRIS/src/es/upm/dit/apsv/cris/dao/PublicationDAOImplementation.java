package es.upm.dit.apsv.cris.dao;

import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

public class PublicationDAOImplementation implements PublicationDAO {

	private static PublicationDAOImplementation instance = null;
	private PublicationDAOImplementation() {}
	public static PublicationDAOImplementation getInstance() {
		if( null == instance ) {
			instance = new PublicationDAOImplementation();
		}
		return instance;
	}

	public Publication create( Publication Publication ) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.save(Publication);
			session.getTransaction().commit();
		} catch (Exception e) {
			// handle exceptions
		} finally {
			session.close();
		}
		return Publication;
	};
	
	public Publication read( String PublicationId ) {
		Publication publication = null;
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.get(Publication.class, PublicationId);
			session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
		return publication;
	};

	public Publication update( Publication Publication ) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(Publication);
			session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
		return Publication;
	};
	
	public Publication delete( Publication Publication ) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(Publication);
			session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
		return Publication;
	};
	
	public List<Publication> readAll(){
		List<Publication> Publications = null;
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			Publications = (List<Publication>) session.createQuery("from Publication").list();
					session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
			session.close();
		}
		return Publications;
	};
	
	public Publication readAsUser(String email, String password) {
		Publication publication = null;
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			publication = (Publication) session.createQuery("select r from Publication r where r.email= :email and r.password = :password")
				.setParameter("email", email)
				.setParameter("password", password)
				.uniqueResult();
			
			session.getTransaction().commit();
		} catch (Exception e) {
		} finally {
		session.close();
	}
		return publication;
	}
};

