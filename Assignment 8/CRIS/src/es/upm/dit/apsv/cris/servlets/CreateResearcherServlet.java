package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class CreateResearcherServlet
 */
@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateResearcherServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if ("true".equals(request.getSession().getAttribute("userAdmin"))){

		String id = request.getParameter("uid");
		String name = request.getParameter("name");
		String lastName = request.getParameter("lastname");

		Researcher r = new Researcher();
		r.setId(id);
		r.setName(name);
		r.setLastname(lastName);
		// guardamos en la BBDD
		ResearcherDAOImplementation.getInstance().create(r);
		response.sendRedirect(request.getContextPath() + "/ResearcherServlet?id=" + r.getId());

		} else {
		request.setAttribute("message", "You are not allowed to view this page");
		getServletContext().getRequestDispatcher("/FormLogin.jsp").forward(request, response);
		}
		}


}
