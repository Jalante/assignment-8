package es.upm.dit.apsv.cris.dao;

import java.util.List;

import es.upm.dit.apsv.cris.model.Researcher;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ResearcherDAOOfyImplementation implements ResearcherDAO {
	
	private static ResearcherDAOOfyImplementation instance = null;
	private ResearcherDAOOfyImplementation() {}
	public static ResearcherDAOOfyImplementation getInstance() {
		if( null == instance ) {
			instance = new ResearcherDAOOfyImplementation();
		}
		return instance;
	}

	@Override
	public Researcher create(Researcher researcher) {
		ofy().save().entity(researcher).now();
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		Researcher researcher = null;
		ofy().load().type(Researcher.class).id(researcherId).now();
		return researcher;
	}

	@Override
	public Researcher update(Researcher researcher) {
		ofy().save().entity(researcher).now();
		return researcher;
	}

	@Override
	public Researcher delete(Researcher researcher) {
		ofy().delete().entity(researcher).now();
		return researcher;
	}

	@Override
	public List<Researcher> readAll() {
		return ofy().load().type(Researcher.class).list();
	}

	@Override
	public Researcher readAsUser(String email, String password) {
		return ofy().load().type(Researcher.class).filter("email", email).first().now();
	}

}
