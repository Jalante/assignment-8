<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="com.google.appengine.api.users.UserService"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory"%>


<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%@ include file="Header.jsp"%>

<% UserService userService = UserServiceFactory.getUserService();%>
<c:if test="${pageContext.request.userPrincipal == null}">
  <p><a href='<%=userService.createLoginURL(request.getRequestURI())%>'>
        Login with Google</a>
  </p>
</c:if>

<c:if test="${pageContext.request.userPrincipal != null}">
  <p><a href='<%=userService.createLogoutURL(request.getRequestURI())%>'>
        Logout from Google</a>
	</p>
</c:if>


	<h2>Acceder a cuenta existente</h2>
	<form action="LoginServlet" method="post">
		<input type="text" name="email" placeholder="Email"> 
		<input type="password" name="password" placeholder="Password">
		<button type="submit">Login</button>
	</form>
	
	
</body>
</html>